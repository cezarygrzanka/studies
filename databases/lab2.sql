-- 22
SELECT
  nazwisko,
  imie,
  punkty,
  (punkty - 5) as bilans
from studenci;

-- 23, 24
SELECT
  st.nazwisko,
  st.imie,
  st.punkty as "punkty z matematyki"
FROM studenci st;
-- 25
SET autocommit=0 -- Wyłączenie autozatwierdzania zmian
START TRANSACTION;
  INSERT INTO studenci
    (imie, nazwisko, punkty)
  VALUES
    ('Taka', 'Transakcja', 5.0);

  SELECT * FROM studenci;

  ROLLBACK;

  SELECT * FROM studenci;

  INSERT INTO studenci
    (imie, nazwisko, punkty)
  VALUES
    ('Taka', 'Tranzakcja', 5.0);

  SELECT * FROM studenci;

  COMMIT;
-- 26
CREATE TABLE emails (
  id INT,
  student_id INT,
  email VARCHAR(25),
  FOREIGN KEY (student_id)
    REFERENCES studenci(id)
) ENGINE=INNODB;

-- 29
CREATE TABLE studenci (
  id INT auto_increment,
  imie VARCHAR(20) DEFAULT "nieznane",
  nazwisko VARCHAR(30) DEFAULT NULL,
  punkty FLOAT DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE emails (
  id INT PRIMARY KEY auto_increment,
  student_id INT,
  email VARCHAR(25),
  FOREIGN KEY (student_id)
    REFERENCES studenci(id)
) ENGINE=INNODB;

INSERT INTO studenci
  (imie, nazwisko, punkty)
VALUES
  ("Krzysztof", "Tomaszewski", 29.5),
  ("Kamil", "Nosowski", 10.0),
  ("Ania", "Matuszak", 16.5),
  ("Weronika", "Szmal", 21.7),
  ("Marcin", "Zamojski", 1.7),
  ("Mieszko", "Miszczak", 18.4);

INSERT INTO emails
  (email, student_id)
VALUES
  ("MarcinZ@baza.pl", 5),
  ("WeronikaS@baza.pl", 4),
  ("AniaM@baza.pl", 3),
  ("KamilN@baza.pl", 2),
  ("KrzysztofT@baza.pl", 1),
  ("KrzysztofT@java.pl", 1),
  ("AniaM@ruby.pl", 3),
  ("AniaM@php.pl", 3);

-- 30
SELECT * FROM studenci
INNER JOIN emails
ON studenci.id = emails.student_id;

-- 31
SELECT * FROM studenci
INNER JOIN emails
WHERE studenci.id = emails.student_id;

-- 32
SELECT
  st.imie,
  st.nazwisko,
  em.email
FROM studenci st
INNER JOIN emails em
ON st.id = em.student_id;

-- 33
CREATE TABLE powiadomienia (
  id INT auto_increment,
  email_id INT,
  komunikat VARCHAR(50),
  data_wyslania TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (email_id)
    REFERENCES emails(id)
) ENGINE=INNODB;

INSERT INTO powiadomienia
  (email_id, komunikat, data_wyslania)
VALUES
  (1, "Zapraszamy z indeksem!", "2020-03-24 10:38:09"),
  (3, "Prosimy o pilny kontakt!", "2020-03-24 10:38:09");

-- 34
SELECT
  st.nazwisko,
  st.imie,
  em.email,
  po.komunikat,
  po.data_wyslania
FROM studenci st
INNER JOIN emails em
ON st.id = em.student_id
INNER JOIN powiadomienia po
ON em.id = po.email_id;

-- 35
SELECT
  st.nazwisko,
  st.imie,
  em.email,
  po.komunikat,
  po.data_wyslania
FROM studenci st
LEFT JOIN emails em
ON st.id = em.student_id
LEFT JOIN powiadomienia po
ON em.id = po.email_id;

-- 36
SELECT
  st.nazwisko,
  st.imie,
  em.email,
  IFNULL(po.komunikat, '') as "komunikat",
  IFNULL(po.data_wyslania, '') as "data_wyslania"
FROM studenci st
LEFT JOIN emails em
ON st.id = em.student_id
LEFT JOIN powiadomienia po
ON em.id = po.email_id;

-- 37
SELECT * FROM studenci LEFT JOIN emails on studenci.id = student_id;
SELECT * FROM emails LEFT JOIN studenci on studenci.id = student_id;

SELECT * FROM studenci RIGHT JOIN emails on studenci.id = student_id;
SELECT * FROM emails RIGHT JOIN studenci on studenci.id = student_id;

-- 38
ALTER TABLE studenci
  ADD COLUMN starosta INT DEFAULT NULL;

UPDATE studenci SET starosta = 2 WHERE imie="Krzysztof" OR imie = "Ania";
UPDATE studenci SET starosta = 6 WHERE imie="Weronika" OR imie = "Marcin";

SELECT
  CONCAT (stu.imie, " ", stu.nazwisko) as "Imie i nazwisko studenta",
  CONCAT (star.imie, " ", star.nazwisko) as "starosta"
FROM studenci stu
INNER JOIN studenci star
ON stu.starosta = star.id;

-- 39
SELECT
  CONCAT (
    stu.imie,
    " ",
    stu.nazwisko
  ) as "Imie i nazwisko studenta",
  IFNULL(
    CONCAT (
      star.imie,
      " ",
      star.nazwisko
    ),
    "STAROSTA"
  ) as "starosta"
FROM studenci stu
LEFT JOIN studenci star
ON stu.starosta = star.id;
