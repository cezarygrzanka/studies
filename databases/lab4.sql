-- 47
-- I
INSERT INTO stypendia
  (student_id, rodzaj, kwota)
VALUES
  (3, 'naukowe', 300),
  (5, 'naukowe', 100),
  (5, 'socjalne', 150);

-- a

SELECT
  nazwisko, imie,
  IFNULL(st.suma, 0) as "razem stypendia naukowe"
FROM studenci
LEFT JOIN (
  SELECT
    student_id,
    sum(kwota) as suma
  FROM stypendia
  WHERE rodzaj = "naukowe"
  GROUP BY student_id
) st ON student_id = studenci.id;

-- b

SELECT
  nazwisko, imie,
  IFNULL(sum(st.kwota), 0) as "razem stypendia naukowe"
FROM studenci
LEFT JOIN stypendia st
ON student_id = studenci.id
WHERE rodzaj = "naukowe"
GROUP BY student_id;

-- II
-- a, b

SELECT
  nazwisko, imie
FROM studenci
LEFT JOIN stypendia st
ON student_id = studenci.id
WHERE rodzaj = "naukowe"
GROUP BY student_id;

-- c
SELECT (
    SELECT nazwisko
    FROM studenci
    WHERE student_id = studenci.id
  ) as nazwisko, (
    SELECT imie
    FROM studenci
    WHERE student_id = studenci.id
  ) as imie
FROM stypendia
GROUP BY student_id
HAVING sum(kwota) > 200;

-- d
SELECT
  nazwisko, imie
FROM studenci
LEFT JOIN stypendia
ON student_id = studenci.id
GROUP BY student_id, studenci.nazwisko
HAVING sum(kwota) > 200;

-- e
SELECT
  nazwisko, imie
FROM studenci
WHERE NOT EXISTS (
  SELECT * FROM stypendia
  WHERE student_id = studenci.id
);

-- III
-- a
SELECT
  nazwisko, imie, (
    SELECT count(kwota)
    FROM stypendia
    WHERE studenci.id = student_id
  ) as "liczba stypendiow"
FROM studenci
ORDER BY nazwisko;

-- b
SELECT
  nazwisko, imie, count(kwota)
FROM studenci
LEFT JOIN stypendia
ON student_id = studenci.id
GROUP BY studenci.id, student_id
ORDER BY nazwisko;

-- 48
-- a
CREATE TABLE informacja_zwrotna
(id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
email_id INT NOT NULL,
info varchar(50) NOT NULL,
data_wyslania TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
CONSTRAINT FOREIGN KEY (email_id)
REFERENCES emails (id) )ENGINE=InnoDB;
INSERT INTO informacja_zwrotna
(email_id,info)
VALUES
(1,'Indeks dostarczony'),
(3,'Brak kontaktu');

-- b
SELECT
  email_id, data_wyslania, komunikat as tresc
FROM powiadomienia
UNION
SELECT
  email_id, data_wyslania, info as tresc
FROM informacja_zwrotna
ORDER BY email_id;

-- c
SELECT
  email, data_wyslania, komunikat as tresc
FROM powiadomienia
INNER JOIN emails
ON powiadomienia.email_id = emails.id
UNION
SELECT
  email, data_wyslania, info as tresc
FROM informacja_zwrotna
INNER JOIN emails
ON informacja_zwrotna.email_id = emails.id
ORDER BY email;

-- d
SELECT
  email_id, data_wyslania, komunikat as tresc
FROM powiadomienia
UNION ALL
SELECT
  email_id, data_wyslania, komunikat as tresc
FROM powiadomienia
ORDER BY email_id;
