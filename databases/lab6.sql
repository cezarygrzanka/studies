-- 53
-- a

DELIMITER //
CREATE FUNCTION ocenaStudenta(n FLOAT)
RETURNS VARCHAR(20)
BEGIN DECLARE ocena VARCHAR(20);
IF n > 10 THEN SET ocena = 'Znakomity';
ELSEIF n <= 10 AND n >= 5 THEN SET ocena = 'Dobry';
ELSE SET ocena = 'Ma potencjal';
END IF;
RETURN ocena;
END //
DELIMITER ;

SELECT imie, nazwisko, ocenaStudenta(punkty) FROM studenci;

-- 54
-- a

CREATE TABLE IF NOT EXISTS studenci_audyt (
  id INT PRIMARY KEY AUTO_INCREMENT,
  student_id INT NOT NULL,
  imie VARCHAR(20),
  nazwisko VARCHAR(30),
  punkty FLOAT,
  data DATETIME DEFAULT NULL,
  akcja VARCHAR(50) DEFAULT NULL
);

DELIMITER //
CREATE TRIGGER before_studenci_update
BEFORE UPDATE ON studenci FOR EACH ROW
BEGIN
INSERT INTO studenci_audyt (student_id, imie, nazwisko, punkty, data, akcja)
VALUES (OLD.id, OLD.imie, OLD.nazwisko, OLD.punkty, NOW(), CONCAT('UPDATE przez', USER()));
END //

CREATE TRIGGER before_studenci_insert
BEFORE INSERT ON studenci FOR EACH ROW
BEGIN
INSERT INTO studenci_audyt (student_id, imie, nazwisko, punkty, data, akcja)
VALUES (NEW.id, NEW.imie, NEW.nazwisko, NEW.punkty, NOW(), CONCAT('INSERT przez', USER()));
END //

CREATE TRIGGER before_studenci_delete
BEFORE DELETE ON studenci FOR EACH ROW
BEGIN
INSERT INTO studenci_audyt (student_id, imie, nazwisko, punkty, data, akcja)
VALUES (OLD.id, OLD.imie, OLD.nazwisko, OLD.punkty, NOW(), CONCAT('DELETE przez', USER()));
END //

DELIMITER ;

INSERT INTO studenci (imie, nazwisko, punkty) VALUES ("Drass", "Paradoslaw", 66);
UPDATE studenci SET nazwisko = "Radaparos" WHERE id = 8;
DELETE FROM studenci WHERE id = 8;
