-- 2
CREATE DATABASE laboratorium;
-- 3
-- A
SHOW DATABASES;
-- B
SHOW DATABASES LIKE "lab%";
-- C -> znak % oznacza dowolną ilość znaków, znak _ oznacza jeden znak
-- 4
DROP DATABASE laboratorium;
SHOW DATABASES;
-- 5
CREATE DATABASE laboratorium CHARACTER SET utf8 COLLATE utf8_general_ci;
-- 6
USE laboratorium;
CREATE TABLE studenci (
  id INT,
  imie VARCHAR(20),
  nazwisko VARCHAR(30),
  PRIMARY KEY (id)
);
-- 7 -> zamiast describe można użyć skróconej wersji -> desc
DESC studenci;
-- 8
INSERT INTO studenci (id, nazwisko) VALUES (0, "Grzanka");
SELECT * FROM studenci;
-- 9 -> błąd otrzymaliśmy, ponieważ domyślną wartością ID jest 0, a ID jest primary key, więc nie mogą istnieć 2 rekordy o tym samym ID
INSERT INTO studenci SET id=0,nazwisko = "Grzanka";
-- POPRAWNE
INSERT INTO studenci SET id = 1, nazwisko = "Grzanka";
-- 10
ALTER TABLE studenci MODIFY COLUMN id INT AUTO_INCREMENT;
INSERT INTO studenci SET nazwisko = "Grzanka";
-- 11
ALTER TABLE studenci
  MODIFY COLUMN imie VARCHAR(20) NOT NULL,
  MODIFY COLUMN nazwisko VARCHAR(30) NOT NULL;
-- 12
INSERT INTO studenci SET nazwisko = "Grzanka";
-- 13
INSERT INTO studenci SET nazwisko = "Grzanka", imie=NULL;
-- 14
ALTER TABLE studenci
  ALTER imie SET DEFAULT 'nieznane';
-- 15
ALTER TABLE studenci
  ADD COLUMN punkty FLOAT DEFAULT 0;
-- 16
TRUNCATE studenci;
INSERT INTO studenci
  (imie, nazwisko, punkty)
VALUES
  ('Jurek', 'Zborowski', 3.4),
  ('Marceli', 'Jurecki', 0.4),
  ('Marcin', 'Mariański', 4.4),
  ('KRYSTIAN', 'ADAMCZYK', 8.2),
  ('Ania', 'Graczyk', 9.99);
-- 17
SELECT * FROM studenci ORDER BY nazwisko;
-- 18
SELECT nazwisko, imie, punkty FROM studenci ORDER BY nazwisko;
-- 19
CREATE TABLE studenci2 SELECT imie, nazwisko FROM studenci;
-- 20
DELETE FROM studenci WHERE id=2;
-- 21
UPDATE studenci SET imie="Injurek" WHERE imie="Jurek";