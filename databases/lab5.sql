-- 50
-- c
SHOW USER();

-- d
CREATE USER cezgrz@localhost IDENTIFIED BY '1234';

-- e
SELECT User, Host FROM mysql.user;

-- g
CREATE USER stefan IDENTIFIED BY '1234';
GRANT SELECT ON laboratorium.* TO stefan;
