-- 41
CREATE VIEW korespondencja
AS
SELECT
  st.nazwisko,
  st.imie,
  em.email,
  IFNULL(po.komunikat, '') as "komunikat",
  IFNULL(po.data_wyslania, '') as "data_wyslania"
FROM studenci st
LEFT JOIN emails em
ON st.id = em.student_id
LEFT JOIN powiadomienia po
ON em.id = po.email_id;

-- 42
SHOW FULL TABLES WHERE table_type = 'VIEW';

DROP VIEW korespondencja;

ALTER
  ALGORITHM=MERGE
VIEW korespondencja AS
  SELECT
    st.nazwisko,
    st.imie,
    em.email,
    IFNULL(po.komunikat, '') as "komunikaty",
    IFNULL(po.data_wyslania, '') as "data_wyslania"
  FROM studenci st
  LEFT JOIN emails em
  ON st.id = em.student_id
  LEFT JOIN powiadomienia po
  ON em.id = po.email_id;

-- 43
-- a
CREATE VIEW najlepsi AS
  SELECT
    imie, nazwisko, punkty
  FROM
    studenci
  WHERE
    punkty > 15;

-- b
CREATE VIEW pozostali AS
  SELECT
    imie, nazwisko, punkty
  FROM
    studenci
  WHERE
    punkty <=15
WITH CHECK OPTION;

-- c, d
INSERT INTO najlepsi
  (imie, nazwisko, punkty)
VALUES
  ('Adam', 'Kowalski', 11);

-- e
INSERT INTO pozostali
  (imie, nazwisko, punkty)
VALUES
  ('Ewa', 'Polak', 17);

-- f
DELETE FROM najlepsi
WHERE punkty < 14;

-- 44
-- a
CREATE TABLE stypendia (
  id INT PRIMARY KEY auto_increment,
  student_id INT,
  rodzaj TEXT,
  kwota INT
)

INSERT INTO stypendia (
  student_id, rodzaj, kwota
) VALUES
(1, "naukowe", 100),
(1, "socjalne", 100),
(3, "inne", 150),
(3, "socjalne", 250),
(3, "naukowe", 250),
(4, "naukowe", 450);

--b
CREATE VIEW stypendia_razem AS
  SELECT
    student_id, sum(kwota) as Razem
  FROM stypendia
  GROUP BY student_id;

--c
CREATE VIEW studenci_stypendia_razem AS
  SELECT
    nazwisko, imie, Razem
  FROM
    stypendia_razem JOIN studenci
    ON student_id = studenci.id
  ORDER BY nazwisko;

--d
CREATE TABLE
  studenci_stypendia_razem_tab
SELECT * FROM studenci_stypendia_razem;

--e
UPDATE studenci_stypendia_razem_tab
SET Razem = 600
WHERE Razem > 600;

--f
UPDATE studenci_stypendia_razem
SET Razem = 600
WHERE Razem > 600;

--45
--a
CREATE TABLE fanoteka (
  id INT auto_increment PRIMARY KEY,
  zespol TEXT,
  album TEXT,
  tytul TEXT
);

INSERT INTO fanoteka
  (zespol, album, tytul)
VALUES
  ('Crazy Band', 'Kosmos', 'Merkury i Wenus' ),
  ('Crazy Band', 'Zegar', 'Minuty i sekundy' ),
  ('Crazy Band', 'Zegar', 'Tik Tak blues' ),
  ('Drive', 'Speed', 'Speed ticket' ),
  ('Drive', 'Speed', 'Lost points'),
  ('Drive', 'Speed', 'Slow down'),
  ('UTP band', 'Baza', 'Moje sprawozdanie :)' );

--b
SELECT DISTINCT zespol
FROM fanoteka
ORDER BY zespol;

--c
SELECT DISTINCT album, zespol
FROM fanoteka
ORDER BY album;

--d
SELECT count(zespol) from fanoteka;
SELECT count(DISTINCT zespol) from fanoteka;
SELECT count(DISTINCT album) from fanoteka;

--e
CREATE TABLE oplaty_za_kola_naukowe (
  id INT auto_increment PRIMARY KEY,
  nazwa TEXT,
  tytulem TEXT,
  oplata INT
);

INSERT INTO oplaty_za_kola_naukowe
  (nazwa, tytulem, oplata)
VALUES
  ('KuJon','materialy do badan',100),
  ('KuJon','bilety',150),
  ('KuJon','sympozjum',250),
  ('Atom','sympozjum',450),
  ('Atom','materialy do badan',337),
  ('UToPia','literatura',80),
  ('OMEGA','materialy do badan',80),
  ('KuJon','materialy do badan',600),
  ('KuJon','bilety',277);

--f
SELECT
  tytulem,
  sum(oplata) as "wydano razem PLN"
FROM oplaty_za_kola_naukowe
GROUP BY tytulem;

--g
SELECT
  nazwa,
  tytulem,
  sum(oplata)
FROM oplaty_za_kola_naukowe
GROUP BY tytulem, nazwa
ORDER BY nazwa;

--h
SELECT
  nazwa,
  tytulem,
  sum(oplata) as "wydano razem PLN"
FROM oplaty_za_kola_naukowe
GROUP BY tytulem, nazwa
HAVING sum(oplata) > 300
ORDER BY nazwa;

--g
SELECT
  nazwa, tytulem, sum(oplata) as "wydano razem PLN"
FROM oplaty_za_kola_naukowe
WHERE tytulem = "materialy do badan"
GROUP BY nazwa
HAVING sum(oplata) > 300;