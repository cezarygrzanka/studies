#include <iostream>
#include <string>

class Ciufciuf {
 private:
  struct Element {
    double value;
    Element* next;
  };

  Element* top;
  Element* bottom;
  int size;

 public:
  Ciufciuf() {
    top = NULL;
    bottom = NULL;
    size = 0;
  }

  bool isEmpty() { return !(bool)size; }

  bool push(double el) {
    Element* n = new Element;
    n->value = el;
    n->next = NULL;

    if (isEmpty()) {
      top = n;
      bottom = n;
    } else {
      top->next = n;
      top = n;
    }

    size++;

    return true;
  }

  double pop() {
    double v = bottom->value;
    Element* p = bottom->next;
    delete bottom;
    bottom = p;
    size--;
    return v;
  }

  void showCiufciuf() {
    Element* now = bottom;

    if (isEmpty()) {
      std::cout << "Ciuf ciuf jest pusty";
    } else {
      for (int i = 0; i < size; i++) {
        std::cout << now->value << "\n";
        now = now->next;
      }
    }
  }
};

class CiufciufPriority {
 private:
  Ciufciuf low;
  Ciufciuf mid;
  Ciufciuf high;
  int size;

 public:
  bool isEmpty() { return !(bool)size; }

  int getSize() { return size; };

  bool push(double el, int p) {
    switch (p) {
      case 0:
        low.push(el);
        break;
      case 1:
        mid.push(el);
        break;
      case 2:
        high.push(el);
      default:
        break;
    }
    size++;
    return true;
  }

  double pop() {
    double v;

    if (!high.isEmpty()) {
      v = high.pop();
    } else if (!mid.isEmpty()) {
      v = mid.pop();
    } else {
      v = low.pop();
    }
    size--;
    return v;
  }

  void shofCiufCiuf() {
    high.showCiufciuf();
    mid.showCiufciuf();
    low.showCiufciuf();
  }
};

int main() {
  Ciufciuf a;
  a.push(3.14);
  a.push(1);
  a.push(2.1);
  a.push(3.14);

  std::cout << a.pop() << "----" << a.pop() << "----";
  std::cout << a.pop() << "----" << a.pop() << "----";
  std::cout << "\n";

  CiufciufPriority b;
  b.push(1, 2);
  b.push(2, 1);
  b.push(3, 0);
  b.push(4, 2);

  std::cout << b.pop() << "----" << b.pop() << "----";
  std::cout << b.pop() << "----" << b.pop() << "----";

  return 0;
}