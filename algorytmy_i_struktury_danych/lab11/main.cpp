#include <algorithm>
#include <iostream>
#include <string>

struct Element {
  double value;
  Element* next;
};

class Stosik {
 private:
  Element* top;
  int size;

 public:
  Stosik() {
    top = NULL;
    size = 0;
  }

  bool isEmpty() { return !(bool)size; }

  bool push(double el) {
    Element* n = new Element;
    n->value = el;

    n->next = isEmpty() ? NULL : top;
    top = n;

    size++;

    return true;
  }

  double pop() {
    double v = top->value;
    Element* p = top->next;
    delete top;
    top = p;
    size--;
    return v;
  }

  void showStosik() {
    Element* now = top;

    if (isEmpty()) {
      std::cout << "Ciuf ciuf jest pusty";
    } else {
      for (int i = 0; i < size; i++) {
        std::cout << now->value << "\n";
        now = now->next;
      }
    }
  }
};

std::string* split(std::string s, char v) {
  std::string* temp = new std::string[s.length()];
  std::string buff = "";
  int ei = 0;

  for (int i = 0; i < s.length(); i++) {
    if ((char)s[i] == v) {
      temp[ei++] = buff;
      buff = "";
    } else {
      buff += s[i];
    }
  }

  temp[ei] = buff;
  std::string* r = new std::string[ei + 1];

  for (int i = 0; i < ei + 1; i++) {
    r[i] = temp[i];
  }

  delete[] temp;

  return r;
}

void calcOnp(std::string onp) {
  Stosik v;
  std::string buff = "";
  for (int i = 0; i < onp.size(); i++) {
    if (onp[i] == ' ') {
      if (buff.length() > 0) {
        v.push(std::stod(buff));
      }
      buff = "";
    } else {
      if ((int)onp[i] >= 48 && (int)onp[i] <= 57) {
        buff += onp[i];
      } else {
        int a = v.pop();
        int b = v.pop();

        std::cout << "a: " << a << " b: " << b << " a" << onp[i] << "b: ";

        switch (onp[i]) {
          case '+':
            std::cout << a + b << std::endl;
            v.push(a + b);
            break;
          case '-':
            std::cout << a - b << std::endl;
            v.push(a - b);
            v.showStosik();
            break;
          case '*':
            std::cout << a * b << std::endl;
            v.push(a * b);
            break;
          case '/':
            std::cout << a / b << std::endl;
            v.push(a / b);
            break;
          default:
            break;
        }
      }
    }
  }
  std::cout << v.pop();
}

int main() {
  Stosik a;
  a.push(10);
  a.push(11);
  a.push(12);
  a.pop();
  // a.showStosik();

  std::string onp = "12 5 - 7 1 + 5 / + 2 *";
  calcOnp(onp);

  return 0;
}
