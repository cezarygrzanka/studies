#include <iostream>

int n;

float* createArray();
void showArray(float* t);
float* boubleSort(float* t);
float* insertionSort(float* t);

int main() {
  float* arr = createArray();
  int choose;

  std::cout << "Przed sortowaniem:\n";
  showArray(arr);

  std::cout << "Ktore sortowanie chcesz wykonac?\n";
  std::cout << "Bouble sort (1)\nInsertion sort (2)\nA więc?: ";
  std::cin >> choose;

  std::cout << "\nSortowanie...\n";
  arr = choose == 1 ? boubleSort(arr) : insertionSort(arr);
  std::cout << "Posortowana:\n";
  showArray(arr);
}

float* createArray() {
  std::cout << "Podaj rozmiar tablicy: ";
  std::cin >> n;

  float* t = new float[n];

  for (int i = 0; i < n; i++) {
    std::cout << "Podaj t[" << i << "]: ";
    std::cin >> t[i];
  }

  return t;
}

void showArray(float* t) {
  for (int i = 0; i < n; i++) {
    std::cout << "t[" << i << "] = " << t[i] << "\n";
  }
}

float* boubleSort(float* t) {
  bool s = false;
  do {
    s = false;

    for (int i = 1; i < n; i++) {
      if (t[i-1] > t[i]) {
        float temp = t[i-1];
        t[i-1] = t[i];
        t[i] = temp;

        s = true;
      }
    }
  } while (s);
  return t;
}

float* insertionSort(float* t) {
  int i = 1;
  while (i < n) {
    int j = i;
    while (j > 0 && t[j-1] > t[j]) {
      float temp = t[j-1];
      t[j-1] = t[j];
      t[j] = temp;
      j--;
    }
    i++;
  }

  return t;
}
