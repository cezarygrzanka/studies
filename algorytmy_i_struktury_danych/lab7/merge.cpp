#include <iostream>

double* createArray(int size);
void showArray(double* t, int size);
double* mergeSort(double* t, int size);
double* merge(double* left, double* right, int sizel, int sizer);
double* slice(double* a, int start, int stop);

int main() {
  int n;

  std::cout << "Podaj rozmiar tablicy: ";
  std::cin >> n;

  double* arr = createArray(n);

  std::cout << "Przed sortowaniem:\n";
  showArray(arr, n);

  std::cout << "\nSortowanie...\n";
  arr = mergeSort(arr, n);
  std::cout << "Posortowana:\n";
  showArray(arr, n);
}

double* createArray(int size) {
  double* t = new double[size];

  for (int i = 0; i < size; i++) {
    std::cout << "Podaj t[" << i << "]: ";
    std::cin >> t[i];
  }

  return t;
}

void showArray(double* t, int size) {
  for (int i = 0; i < size; i++) {
    std::cout << "t[" << i << "] = " << t[i] << "\n";
  }
}

double* slice(double* a, int start, int stop) {
  double* r = new double[stop - start];
  int p = 0;

  for (int i = start; i < stop; i++) {
    r[p++] = a[i];
  }

  return r;
}

double* merge(double* left, double* right, int sizel, int sizer) {
  double* merged = new double[sizel + sizer];
  int li = 0, ri = 0, p = 0;

  while (li < sizel && ri < sizer) {
    if (left[li] < right[ri]) {
      merged[p++] = left[li++];
    } else {
      merged[p++] = right[ri++];
    }
  }

  while (li < sizel)
    merged[p++] = left[li++];
  while (ri < sizer)
    merged[p++] = right[ri++];

  return merged;
}

double* mergeSort(double* t, int size) {
  if (size <= 1) {
    return t;
  }

  int p = size / 2;
  int sl = p;
  int sr = size - p;
  double* left = new double[sl];
  double* right = new double[sr];

  left = slice(t, 0, p);
  right = slice(t, p, size);

  left = mergeSort(left, sl);
  right = mergeSort(right, sr);

  return merge(left, right, sl, sr);
}