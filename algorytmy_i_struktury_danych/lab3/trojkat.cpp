#include <iostream>
#include <cmath>

int main() {
  double a, b, c, p;
  std::cout << "Podaj a: "; std::cin >> a;
  std::cout << "Podaj b: "; std::cin >> b;
  std::cout << "Podaj c: "; std::cin >> c;

  if (a+b > c && a+c > b && b+c > a) {
    p = (a + b + c) / 2;

    std::cout << "Obwod wynosi: " << (a + b + c) << "\n";
    std::cout << "Pole wynosi : " << std::sqrt(p * (p-a) * (p-b) * (p-c)) << "\n";
  } else {
    std::cout << "Trojak jest niemozliwy do utworzenia\n";
  }

  return 0;
}