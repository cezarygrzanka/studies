#include <iostream>
#include <cstdlib>
#include <ctime>

int main() {
  srand( time( NULL ) );
  int random = (std::rand() % 14) + 1, a;
  std::cout << "Wylosowalem liczbe, teraz ja zgadnij\n";

  do {
    std::cout << "Podaj liczbe: ";
    std::cin >> a;
    if (a != random) std::cout << ((random > a) ? "Twoja liczba jest za mala!\n" : "Twoja liczba jest za duza!\n");
  } while (random != a);

  std::cout << "Gratulacje! Zgdales, liczba to: " << random << "\n";

  return 0;
}