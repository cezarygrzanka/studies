#include <iostream>

int** wypelnij();
void wyswietl(int** arr);

int main() {
  std::cout << "Wypelnij macierz kwadratowa\n";
  int** macierz = wypelnij();
  wyswietl(macierz);

  int wyznacznik = ((macierz[0][0] * macierz[1][1]) - (macierz[0][1] * macierz[1][0]));

  std::cout << "Wyznacznik macierzy to: " << wyznacznik << "\n";

  return 0;
}

int** wypelnij() {
  int** temp = 0;
  temp = new int*[2];

  for (int i = 0; i < 2; i++) {
    temp[i] = new int[2];
    for (int j = 0; j < 2; j++) {
      std::cout << "Wypelnij " << i << " " << j << ": ";
      std::cin >> temp[i][j];
    }
  }

  return temp;
}

void wyswietl(int** arr) {
  std::cout << "\n";
  for(int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      std::cout << arr[i][j] << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}
