#include <iostream>

int** wypelnij();
void wyswietl(int** arr);

int main() {
  std::cout << "Wypelnij pierwsza macierz kwadratowa\n";

  std::cout << "Wypelnianie macierzy nr. 1\n";
  int** macierz1 = wypelnij();
  std::cout << "Wypelnianie macierzy nr. 2\n";
  std::cout << "\n";
  int** macierz2 = wypelnij();

  int** macierz3 = new int*[2];
  macierz3[0] = new int[2];
  macierz3[1] = new int[2];

  macierz3[0][0] = (macierz1[0][0] * macierz2[0][0]) + (macierz1[0][1] * macierz2[1][0]);
  macierz3[0][1] = (macierz1[0][0] * macierz2[0][1]) + (macierz1[0][1] * macierz2[1][1]);
  macierz3[1][0] = (macierz1[1][0] * macierz2[0][0]) + (macierz1[1][1] * macierz2[1][0]);
  macierz3[1][1] = (macierz1[1][0] * macierz2[0][1]) + (macierz1[1][1] * macierz2[1][1]);

  std::cout << "Wynik mnożenia macierzy:\nmacierz1";
  wyswietl(macierz1);
  std::cout << "Oraz macierz2";
  wyswietl(macierz2);
  std::cout << "to: ";
  wyswietl(macierz3);

  return 0;
}

int** wypelnij() {
  int** temp = 0;
  temp = new int*[2];

  for (int i = 0; i < 2; i++) {
    temp[i] = new int[2];
    for (int j = 0; j < 2; j++) {
      std::cout << "Wypelnij " << i << " " << j << ": ";
      std::cin >> temp[i][j];
    }
  }

  return temp;
}

void wyswietl(int** arr) {
  std::cout << "\n";
  for(int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      std::cout << arr[i][j] << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}
