#include <iostream>

int** wypelnij();
void wyswietl(int** arr);

int main() {
  std::cout << "Wypelnij macierz kwadratowa\n";
  int** macierz = wypelnij();

  std::cout << "Oto macierz transponowana z tej macierzy\n";
  wyswietl(macierz);

  return 0;
}

int** wypelnij() {
  int** temp = 0;
  temp = new int*[2];

  for (int i = 0; i < 2; i++) {
    temp[i] = new int[2];
    for (int j = 0; j < 2; j++) {
      std::cout << "Wypelnij " << i << " " << j << ": ";
      std::cin >> temp[i][j];
    }
  }

  return temp;
}

void wyswietl(int** arr) {
  std::cout << "\n";
  for(int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      std::cout << arr[j][i] << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}
