#include <iostream>

int main() {
  int a, b;
  std::cout << "Podaj liczbę a: ";
  std::cin >> a;

  std::cout << "Podaj liczbę b: ";
  std::cin >> b;

  std::cout << "Suma wynosi: " << a + b << "\n";
  std::cout << "Roznica wynosi: " << a - b << "\n";
  std::cout << "Iloczyn wynosi: " << a * b << "\n";
  if (b == 0) std::cout << "Nie dziel przez 0!" << "\n";
  else std::cout << "Iloraz wynosi: " << a / b << "\n";
  return 0;
}