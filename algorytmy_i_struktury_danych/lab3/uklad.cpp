#include <iostream>
#include <cmath>

int main() {
  double x1, y1, x2, y2, z1, z2, W, Wx, Wy, x, y;
  std::cout << "| X1x + Y1y = Z1\n";
  std::cout << "| X2x + Y2y = Z2\n\n";

  std::cout << "Podaj X1: "; std::cin >> x1;
  std::cout << "Podaj Y1: "; std::cin >> y1;
  std::cout << "Podaj Z1: "; std::cin >> z1;
  std::cout << "Podaj X2: "; std::cin >> x2;
  std::cout << "Podaj Y2: "; std::cin >> y2;
  std::cout << "Podaj Z2: "; std::cin >> z2;
  std::cout << "\n";
  std::cout << "| " << x1 << "x + " << y1 <<"y = "<< z1 << "\n";
  std::cout << "| " << x2 << "x + " << y2 <<"y = "<< z2 << "\n\n";

  W = (x1 * y2) - (x2 * y1);
  Wx = (z1 * y2) - (z2 * y1);
  Wy = (x1 * z2) - (x2 * z1);

  if (W == 0) {
    std::cout << "Uklad jest nieoznaczony lub sprzeczny";
  } else {
    std::cout << "Uklad jest oznaczony i jego rozwiazanie to:\n";
    x = Wx / W;
    y = Wy / W;

    std::cout << "x = " << x << "\n";
    std::cout << "x = " << y << "\n";
  }

  return 0;
}
