#include <iostream>
#include <math.h>

int main() {
  int liczba;
  float pierw;

  std::cout << "Podaj liczbe: ";
  std::cin >> liczba;

  pierw = std::sqrt(liczba);

  if (liczba == 2) {
    std::cout << "Podana liczba: " << liczba << " jest liczba pierwsza\n";
    return 0;
  } else if (liczba < 2) {
    std::cout << "Liczba musi byc wieksza od 2!";
    return 0;
  } else {
    for (int i = 2; i <= pierw; i++) {
      if (liczba % i == 0) {
        std::cout << "Liczba: " << liczba << " NIE jest liczba piersza\n";
        return 0;
      }
    }
  }

  std::cout << "Liczba: " << liczba << " jest liczba piersza\n";
  return 0;
}