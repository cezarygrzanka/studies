#include <iostream>

int main() {
    long long a = 0, b = 1;

    std::cout << "Podaj ile chcesz liczb fibbonaciego: ";
    int n;
    std::cin >> n;
    for(int i = 0; i < n; i++) {
        std::cout << b << "\n";
        b += a;
        a = b-a;
    }
    return 0;
}