#include <iostream>

void iter(int number);
void reku(int number, unsigned long long result = 1);

int main() {
  int choose, number;

  std::cout << "Podaj liczbę, której silnie chcesz obliczyć: ";
  std::cin >> number;

  std::cout << "Wykonac iteracyjnie czy rekurencyjnie? (1, 2): ";
  std::cin >> choose;

  switch (choose) {
    case 1:
      iter(number);
      break;
    case 2:
      reku(number);
      break;
  }
  return 0;
}

void iter(int number) {
  unsigned long long result = 1;
  int temp = number;

  while (temp > 0) result *= temp--;

  std::cout << "Silnia to " << result << "\n";
}

void reku(int number, unsigned long long result) {
  unsigned long long temp = result;

  if (number > 0) {
    temp *= number;
    reku(--number, temp);
  } else {
    std::cout << "Silnia to " << temp << "\n";
  }
}
