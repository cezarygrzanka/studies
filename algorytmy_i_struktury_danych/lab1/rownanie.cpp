#include <iostream>
#include <math.h>

int main() {
  float a = 1, b = 1, c = 1,
        delta, d,
        x1, x2;

  std::cout << "Podaj poszczególne skladniki\n";
  std::cout << "a: ";
  std::cin >> a;
  std::cout << "b: ";
  std::cin >> b;
  std::cout << "c: ";
  std::cin >> c;

  if (a == 0) {
    std::cout << "Podales rownanie liniowe, wyglada ono nastepujaco: ";
    std::cout << b << "x" << (c >= 0 ? "+" : "") << c << "\n";

    x1 = -c / b;
    std::cout << "Wynik to: x = " << x1 << "\n";
    return 0;
  } else {
    std::cout << "Podales rownanie kwadratowe, wyglada ono nastepujaco: ";
    std::cout << a << "x^2" << (b >= 0 ? "+" : "") << b << "x" << (c >= 0 ? "+" : "") << c << "\n";

    delta = (b * b) - (4 * a * c);

    if (delta > 0) {
      x1 = (-b - std::sqrt(delta)) / (2 * a);
      x2 = (-b + std::sqrt(delta)) / (2 * a);

      std::cout << "Wynik to: x1 = " << x1 << ", x2 = " << x2 << "\n";
    } else if (delta == 0) {
      x1 = (-b - std::sqrt(delta)) / (2 * a);

      std::cout << "Wynik to: x = " << x1 << "\n";
    } else if (delta < 0) {
      x1 = (-b) / (2 * a);
      x2 = (-b) / (2 * a);

      d = (std::sqrt(-delta) / (2 * a));

      std::cout << "Wynik to: x1 = " << x1 << " - " << d << "i\n";
      std::cout << "          x2 = " << x2 << " + " << d << "i\n";
    }
  }
}