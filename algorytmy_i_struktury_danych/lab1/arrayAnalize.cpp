#include <iostream>
#include <math.h>

int main() {
  int n, min, max,
      sr, s, u, p3,
      i = 1, war = 0;

  std::cout << "Podaj n: ";
  std::cin >> n;

  int t[n];

  std::cout << "Podaj pierwszy element tablicy: ";
  std::cin >> t[0];

  min = t[0];
  max = t[0];
  sr = t[0];
  s = t[0];

  if (t[0] % 3 == 0) {
    p3 = 1;
  } else {
    p3 = 0;
  }

  while (i < n) {
    std::cout << "Podaj kolejny element tablicy: ";
    std::cin >> t[i];

    if (t[i] < min) min = t[i];
    if (t[i] > max) max = t[i];

    if (t[i] % 3 == 0) p3++;

    sr += t[i];
    s += t[i];
    i++;
  }

  sr /= n;
  i = 0;

  while (i < n) {
    war = war + ((t[i] - sr) * (t[i] - sr));
    i++;
  };

  war /= n;

  float ods = sqrt(war);

  std::cout << "Wartosc minimalna to:   " << min << "\n";
  std::cout << "Wartosc maksymalna to:  " << max << "\n";
  std::cout << "Rozrzut wynosi:         " << (max - min) << "\n";
  std::cout << "Srednia wartosc wynosi: " << sr << "\n";
  std::cout << "Suma wartosci wynosi:   " << s << "\n";
  std::cout << "Odchylenie standardowe: " << ods << "\n";
  std::cout << "Warjancja wynosi:       " << war << "\n";
  std::cout << "Liczb 3| jest:          " << p3 << "\n";

  int x;
  std::cout << "Podaj x: ";
  std::cin >> x;

  std::cout << "Element " << x << " ma wartosc tablica[x] = " << t[x-1];

  return 0;
}
