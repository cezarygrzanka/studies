#include <iostream>

int main() {
  int a, b;

  std::cout << "Podaj liczbe, i potege do ktorej chesz ja podniesc\n";
  std::cout << "liczba: ";
  std::cin >> a;
  std::cout << "potega: ";
  std::cin >> b;

  if (b == 0) {
    std::cout << "Wynik to 1";
    return 0;
  }

  while (b > 1) {
    b--;
    a *= a;
  }

  std::cout << "Wynik to " << a;
  return 0;
}