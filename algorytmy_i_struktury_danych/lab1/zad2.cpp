#include <iostream>

void nww(int a, int b);
void nwd(int a, int b);

int main() {
  int x, y, a, b, p, nww;

  std::cout << "Podaj liczbę a: ";
  std::cin >> x;
  std::cout << "Podaj liczbę b: ";
  std::cin >> y;

  if (x>y) {
    a = x;
    b = y;
  } else {
    a = y;
    b = x;
  }

  while (a % b != 0) {
    p = a % b;
    a = b;
    b = p;
  }

  nww = (x / b) * y;

  std::cout << "NWD to: " << b << ", NWW to: " << nww << "\n";
}

