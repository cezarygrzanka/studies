#include <iostream>
#include <string>

int main() {
  int sum = 0, i;
  std::string a;

  std::cout << "Podaj liczbe, ktorej cyfry chcialbys zsumowac: ";
  std::cin >> a;

  i = a.length() - 1;


  while (i >= 0) sum += ((int)a[i--] - 48);

  std::cout << "Suma cyfr to: " << sum << "\n";
}