#include <stdlib.h>
#include <time.h>

#include <iostream>
#include <string>

double* generateDataset(int size, std::string type);
void swap(double* arr, int a, int b);
double hoare(double* arr, int left, int right, double k);

int pivot[10];

int main() {
  int n = 10;
  double k;
  // double* arr = new double[n];

  // arr = generateDataset(n, "rand");

  double arr[10] = {5, 3, 2, 7, 10, 33, 11, 4, 7, 9};

  std::cout << "Podaj k: ";
  std::cin >> k;

  std::cout << hoare(arr, 0, n - 1, k);

  // delete[] arr;
  return 0;
}

double* generateDataset(int size, std::string type) {
  srand(time(NULL));

  double* arr = new double[size];

  for (int i = 0; i < size; i++) {
    arr[i] = type != "rand" ? i : std::rand() % size;
  }

  return arr;
}

double hoare(double* arr, int left, int right, double k) {
  if (left == right) {
    return arr[left];
  }

  int i = (left + right) / 2;
  int j = left;
  double pivot = arr[i];
  arr[i] = arr[right];
  int a = left;

  while (a < right) {
    if (arr[a] < pivot) {
      swap(arr, a++, j++);
    } else {
      a++;
    }
  }

  arr[right] = arr[j];
  arr[j] = pivot;
  int r = j - left + 1;

  if (r >= k) {
    right = left + r - 1;
  }
  if (r < k) {
    k -= r;
    left += r;
  }
  hoare(arr, left, right, k);
}

void swap(double* arr, int a, int b) {
  double temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}