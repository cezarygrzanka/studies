#include <iostream>

void czy(int liczba) {
  int licz = 0;
  for (int i = 1; i < liczba; i++)
    if (liczba % i == 0) licz += i;

  if (liczba == licz)
    std::cout << "Liczba " << liczba << " jest Doskonala!\n";
}

int main() {
  for (int i = 2; i <= 30; i++) czy(i);

  return 0;
}