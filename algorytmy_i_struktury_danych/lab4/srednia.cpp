#include <iostream>

int main() {
  int n;
  std::cout << "Podaj ilosc liczb: ";
  std::cin >> n;
  float avg = 0;

  for (int i = 0; i < n; i++) {
    float l, w;
    std::cout << "Podaj liczbe: ";
    std::cin >> l;
    std::cout << "Podaj wage: ";
    std::cin >> w;
    avg += w * l;
    std::cout << "\n";
  }

  std::cout << "Srednia wazona z podanych liczb wynosi: " << (avg / n) << "\n";
}