#include <iostream>
#include <cstring>
#include <cmath>

void decToHex();
void hexToDec();

int main() {
  int choose;
  std::cout << "Zamierzasz konwertować z hexadecymalnych na decymalne? (1)\n";
  std::cout << "czy z decymalnych na hexadecymalne? (2)\nWybor: ";
  std::cin >> choose;

  choose == 1 ? hexToDec() : decToHex();
  return 0;
}

void decToHex() {
  std::string code = "0123456789ABCDEF";

  int number, temp;
  std::cout << "Podaj liczbę w systemie dziesiętnym: ";
  std::cin >> number;
  temp = number;
  std::string res = "";

  while (temp > 0) {
    res = code[temp % 16] + res;
    temp /= 16;
  }

  std::cout << "Liczba " << number << "(10) wynosi: " <<  res << "(16)\n";
};

void hexToDec() {
  std::string code = "0123456789ABCDEF";
  std::string number;
  std::cout << "Podaj liczbę w systemie hexadecymalnym: ";
  std::cin >> number;

  int len = number.length(), res = 0;

  for (int i = 0; i < len; i++ )
    res += code.find(number[i]) * pow(16, len - i - 1);

  std::cout << "Liczba " << number << "(16) wynosi: " << res << "(10)\n";
}