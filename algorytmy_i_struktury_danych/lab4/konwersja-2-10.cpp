#include <iostream>
#include <cstring>
#include <cmath>

void binToDec();
void decToBin();


int main() {
  int choose;
  std::cout << "Zamierzasz konwertować z binarnych na decymalne? (1)\n";
  std::cout << "czy z decymalnych na binarne? (2)\nWybor: ";
  std::cin >> choose;

  choose == 1 ? binToDec() : decToBin();

  return 0;
}

void binToDec() {
  std::string number;
  std::cout << "Podaj liczbę w systemie binarnym: ";
  std::cin >> number;

  int len = number.length(), res = 0;

  for (int i = 0; i < len; i++) {
    int num = (int)number[i] - '0';
    res += num * pow(2, len - i - 1);
  }

  std::cout << "Liczba " << number << "(2) wynosi: " << res << "(10)\n";
};

void decToBin() {
  int number, temp;
  std::cout << "Podaj liczbę w systemie dziesiętnym: ";
  std::cin >> number;
  temp = number;
  std::string res = "";

  while (temp > 0) {
    res = (temp % 2 ? "1" : "0") + res;
    temp /= 2;
  }

  std::cout << res << "\n";
};

