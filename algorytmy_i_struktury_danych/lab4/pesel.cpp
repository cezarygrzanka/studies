#include <iostream>

int main() {
  int sum = 0;
  int dict[11] = {1,3,7,9,1,3,7,9,1,3,1};

  std::string pesel;
  std::cout << "Podaj pesel: ";
  std::cin >> pesel;

  if (pesel.length() != 11) {
    std::cout << "Podano nieprawidlowy pesel\n";
    return 0;
  }

  for (int i = 0; i < 11; i++) {
    sum += ((int)pesel[i] - '0') * dict[i];
  }

  if (sum % 10 == 0) {
    std::cout << "Pesel jest prawidłowy, suma to: " << sum << "\n";
  } else {
    std::cout << "Pesel jest nieprawidłowy, suma to: " << sum << "\n";
  }

  return 0;
}