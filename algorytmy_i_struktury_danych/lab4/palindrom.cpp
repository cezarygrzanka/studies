#include <iostream>

int main() {
  std::string str;
  std::cout << "Podaj napis który chcesz przetestować: ";
  std::cin >> str;

  int len = str.length();

  for (int i = 0; i < (len / 2); i++) {
    if (str[i] != str[len - i - 1]) {
      std::cout << "Wyraz "<< str <<" nie jest palindromem!\n";
      return 0;
    }
  }

  std::cout << "Wyraz " << str << " jest palindromem!\n";
  return 0;
}