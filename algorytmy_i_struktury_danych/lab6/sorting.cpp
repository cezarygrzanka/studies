#include <iostream>

int n;

float* createArray();
void showArray(float* t, int size);
float* selectionSort(float* t);
float* quickSort(float* t, int size);
float* connectArrays(float* l, float* r, int sizel, int sizer);

int main() {
  float* arr = createArray();
  int choose;

  std::cout << "Przed sortowaniem:\n";
  showArray(arr, n);

  std::cout << "Ktore sortowanie chcesz wykonac?\n";
  std::cout << "Selection sort (1)\nQuick sort (2)\nA więc?: ";
  std::cin >> choose;

  std::cout << "\nSortowanie...\n";
  arr = choose == 1 ? selectionSort(arr) : quickSort(arr, n);
  std::cout << "Posortowana:\n";
  showArray(arr, n);
}

float* createArray() {
  std::cout << "Podaj rozmiar tablicy: ";
  std::cin >> n;

  float* t = new float[n];

  for (int i = 0; i < n; i++) {
    std::cout << "Podaj t[" << i << "]: ";
    std::cin >> t[i];
  }

  return t;
}

void showArray(float* t, int size) {
  for (int i = 0; i < size; i++) {
    std::cout << "t[" << i << "] = " << t[i] << "\n";
  }
}

float* selectionSort(float* t) {
  for (int i = 1; i < n; i++) {
    float min = t[i - 1];
    int j = i;

    while (j < n) {
      if (t[j] < min) {
        float temp = t[i - 1];
        t[i - 1] = t[j];
        t[j] = temp;
        min = t[j];
      }
      j++;
    }
  }

  return t;
}

float* connectArrays(float* l, float* r, int sizel, int sizer) {
  int i = 0, j = 0;
  float* t = new float;

  while (j < sizel)
    t[i++] = l[j++];
  j = 0;
  while (j < sizer)
    t[i++] = r[j++];

  return t;
}

float* quickSort(float* t, int size) {
  float min = t[0], max = t[0];

  if (size == 1)
    return t;

  for (int i = 1; i < size; i++) {
    min = t[i] < min ? t[i] : min;
    max = t[i] > max ? t[i] : max;
  }

  if (min == max)
    return t;

  float pivot = (min + max) / 2;

  float* l = new float;
  float* r = new float;

  int li = 0, ri = 0;

  for (int i = 0; i < size; i++) {
    if (t[i] <= pivot)
      l[li++] = t[i];
    else
      r[ri++] = t[i];
  }

  return connectArrays(quickSort(l, li), quickSort(r, ri), li, ri);
}