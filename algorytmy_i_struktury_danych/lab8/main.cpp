#include <fstream>
#include <iostream>

#include "algs/algs.h"

void performTest(int* sizes, int count, int no);
void performSorting(double* alg(double* t, int size),
                    std::string name,
                    double* opti,
                    double* pesi,
                    double* rand,
                    int size,
                    int no);

int main() {
  system("exec rm ./results/*");
  system("exec rm ./data/*");

  char choose = 'a';
  int count = 8, repeats = 0;
  std::string sorts[6] = {"bbl", "sel", "ins", "qck", "mrg"};

  std::cout << "Manual or auto?\nauto is designed for lab8 :P\nso which one? "
               "[m, A]: ";
  std::cin >> choose;
  int* sizes = new int[count];

  if (choose == 'a' || choose == 'A') {
    repeats = 5;
    sizes[0] = 100;
    sizes[1] = 500;
    sizes[2] = 1000;
    sizes[3] = 5000;
    sizes[4] = 10000;
    sizes[5] = 20000;
    sizes[6] = 60000;
    sizes[7] = 100000;

    for (int i = 0; i < repeats; i++) {
      std::cout << "\n\nDeleting old data...\n";
      system("exec rm ./data/*");
      std::cout << "Generating...\n";
      std::string destination = generateDataset("data/", sizes, count);
      std::cout << "Dataset generated in " << destination << "\n";

      performTest(sizes, count, i);
    }
  } else {
    std::cout << "How many values? [eg. 5]: ";

    std::cin >> count;
    repeats = count;
    delete[] sizes;
    sizes = new int[count];

    for (int i = 0; i < count; i++) {
      std::cout << "Type next count of values: ";
      std::cin >> sizes[i];
    }

    std::cout << "\n\nDeleting old data...\n";
    std::cout << "Generating...\n";
    std::string destination = generateDataset("data/", sizes, count);
    std::cout << "Dataset generated in " << destination << "\n";

    performTest(sizes, count, 0);
  }

  std::cout << "Calculating results...";
  calculateResults(sizes, sorts, repeats, count);

  return 0;
}

void performTest(int* sizes, int count, int no) {
  for (int s = 0; s < count; s++) {
    double* optiA = new double[sizes[s]];
    double* pesiA = new double[sizes[s]];
    double* randA = new double[sizes[s]];
    int i = 0;

    std::ifstream opti("data/" + std::to_string(sizes[s]) + ".opti");
    std::ifstream pesi("data/" + std::to_string(sizes[s]) + ".pesi");
    std::ifstream rand("data/" + std::to_string(sizes[s]) + ".rand");

    std::string lo, lp, lr;  // line opti, line pesi, line rand

    while (getline(opti, lo) && getline(pesi, lp) && getline(rand, lr)) {
      optiA[i] = stoi(lo);
      pesiA[i] = stoi(lp);
      randA[i] = stoi(lr);
      i++;
    }

    if (sizes[s] <= 50000) {
      performSorting(bouble, "bbl", optiA, pesiA, randA, sizes[s], no);
      performSorting(selection, "sel", optiA, pesiA, randA, sizes[s], no);
      performSorting(insertion, "ins", optiA, pesiA, randA, sizes[s], no);
    }
    performSorting(quickSort, "qck", optiA, pesiA, randA, sizes[s], no);
    performSorting(mergeSort, "mrg", optiA, pesiA, randA, sizes[s], no);

    opti.close();
    pesi.close();
    rand.close();

    delete[] optiA;
    delete[] pesiA;
    delete[] randA;
  }
}

void performSorting(double* alg(double* t, int size),
                    std::string name,
                    double* opti,
                    double* pesi,
                    double* rand,
                    int size,
                    int no) {
  double* o = new double[size];
  double* p = new double[size];
  double* r = new double[size];
  float co, cp, cr;

  for (int i = 0; i < size; i++) {
    o[i] = opti[i];
    p[i] = pesi[i];
    r[i] = rand[i];
  }

  std::ofstream fopti("results/" + name + "." + std::to_string(size) + ".opti",
                      std::ios::app);
  std::ofstream fpesi("results/" + name + "." + std::to_string(size) + ".pesi",
                      std::ios::app);
  std::ofstream frand("results/" + name + "." + std::to_string(size) + ".rand",
                      std::ios::app);

  std::cout << "Performing optimistitc with: " << name << " on: " << size
            << " numbers...\n";
  clock_t start = clock();
  alg(o, size);
  clock_t stop = clock();
  co = stop - start;
  fopti << co / CLOCKS_PER_SEC << "\n";
  delete[] o;

  std::cout << "Performing pesimistic  with: " << name << " on: " << size
            << " numbers...\n";
  start = clock();
  alg(p, size);
  stop = clock();
  cp = stop - start;
  fpesi << cp / CLOCKS_PER_SEC << "\n";
  delete[] p;

  std::cout << "Performing random      with: " << name << " on: " << size
            << " numbers...\n";
  start = clock();
  alg(r, size);
  stop = clock();
  cr = stop - start;
  frand << cr / CLOCKS_PER_SEC << "\n";
  delete[] r;

  fopti.close();
  fpesi.close();
  frand.close();
}
