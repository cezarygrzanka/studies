#include <iomanip>
#include <iostream>
#include <string>

#include "algs/algs.h"

int main() {
  int sizes[1] = {100};
  std::string sorts[5] = {"bbl", "sel", "ins", "qck", "mrg"};
  calculateResults(sizes, sorts, 10, 1);
  return 0;
}