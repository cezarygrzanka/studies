double* connectArrays(double* l, double* r, int sizel, int sizer) {
  int i = 0, j = 0;
  double* t = new double[sizel + sizer];

  while (j < sizel)
    t[i++] = l[j++];
  j = 0;
  while (j < sizer)
    t[i++] = r[j++];

  delete[] l;
  delete[] r;

  return t;
}

double* quickSort(double* t, int size) {
  double min = t[0], max = t[0];

  if (size == 1)
    return t;

  for (int i = 1; i < size; i++) {
    min = t[i] < min ? t[i] : min;
    max = t[i] > max ? t[i] : max;
  }

  if (min == max)
    return t;

  double pivot = (min + max) / 2;

  double* l = new double[size];
  double* r = new double[size];

  int li = 0, ri = 0;

  for (int i = 0; i < size; i++) {
    if (t[i] <= pivot)
      l[li++] = t[i];
    else
      r[ri++] = t[i];
  }

  return connectArrays(quickSort(l, li), quickSort(r, ri), li, ri);
}