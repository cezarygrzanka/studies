double* selection(double* t, int size) {
  for (int i = 1; i < size; i++) {
    float min = t[i - 1];
    int j = i;

    while (j < size) {
      if (t[j] < min) {
        float temp = t[i - 1];
        t[i - 1] = t[j];
        t[j] = temp;
        min = t[j];
      }
      j++;
    }
  }

  return t;
}