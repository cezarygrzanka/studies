#include "bouble.cpp"
#include "generate.cpp"
#include "insertion.cpp"
#include "mergeSort.cpp"
#include "quickSort.cpp"
#include "selection.cpp"
#include "utils.cpp"

double* bouble(double* t, int size);
double* insertion(double* t, int size);
double* selection(double* t, int size);
double* quickSort(double* t, int size);
double* mergeSort(double* t, int size);
double* createArray(int size);
void showArray(double* t, int size);
std::string generateDataset(std::string basePath, int* sizes, int size);
void calculateResults(int* sizes, std::string* sorts, int repeats, int count);