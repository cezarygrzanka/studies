#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>

std::string generateDataset(std::string basePath, int* sizes, int size) {
  srand(time(NULL));

  for (int s = 0; s < size; s++) {
    std::ofstream opti;
    std::ofstream pesi;
    std::ofstream rand;

    opti.open(basePath + std::to_string(sizes[s]) + ".opti");
    pesi.open(basePath + std::to_string(sizes[s]) + ".pesi");
    rand.open(basePath + std::to_string(sizes[s]) + ".rand");

    for (int i = 0; i < sizes[s]; i++) {
      opti << i << "\n";
      pesi << sizes[s] - i << "\n";
      rand << std::rand() % 1000000 << "\n";
    }

    opti.close();
    pesi.close();
    rand.close();

    std::cout << "Genereted set for " << sizes[s] << " numbers as " << sizes[s]
              << ".{rand,opti,pesi}\n";
  }

  return basePath;
}