#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

double* createArray(int size) {
  double* t = new double[size];

  for (int i = 0; i < size; i++) {
    std::cout << "Podaj t[" << i << "]: ";
    std::cin >> t[i];
  }

  return t;
}

void showArray(double* t, int size) {
  for (int i = 0; i < size; i++) {
    std::cout << "t[" << i << "] = " << t[i] << "\n";
  }
}

void generateBorder(int repeats, std::ofstream& resfile) {
  std::cout << "+------+-------+-------------------+----------+";
  for (int i = 1; i < repeats; i++)
    std::cout << "----------+";
  std::cout << "----------+\n";

  resfile << "+------+-------+-------------------+----------+";
  for (int i = 1; i < repeats; i++)
    resfile << "----------+";
  resfile << "----------+\n";
}

void generateHeaders(int repeats, std::ofstream& resfile) {
  generateBorder(repeats, resfile);
  std::cout << "| Name | Diff. |      numbers      |    p0    |";
  for (int i = 1; i < repeats; i++)
    std::cout << "    "
              << "p" << i << "    |";
  std::cout << "   avg    |\n";

  resfile << "| Name | Diff. |      numbers      |    p0    |";
  for (int i = 1; i < repeats; i++)
    resfile << "    "
            << "p" << i << "    |";
  resfile << "   avg    |\n";
  generateBorder(repeats, resfile);
}

void displayName(std::string name, std::ofstream& resfile) {
  std::cout << std::setw(7) << std::setfill(' ') << std::left << "| " + name
            << std::right << '|';

  resfile << std::setw(7) << std::setfill(' ') << std::left << "| " + name
          << std::right << '|';
}

void displaySize(int size, std::ofstream& resfile) {
  std::cout << std::setw(19) << std::setfill(' ') << std::left
            << " " + std::to_string(size) << '|';

  resfile << std::setw(19) << std::setfill(' ') << std::left
          << " " + std::to_string(size) << '|';
}

void displayResNumber(double n, std::ofstream& resfile) {
  std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(7)
            << std::setw(10) << std::setfill(' ') << std::left << n
            << "\x1b[0m|";

  resfile << std::setiosflags(std::ios::fixed) << std::setprecision(7)
          << std::setw(10) << std::setfill(' ') << std::left << n << "|";
}

void calculateResults(int* sizes, std::string* sorts, int repeats, int count) {
  std::ofstream resfile("res.table");

  std::cout << "\n\n\n";
  generateHeaders(repeats, resfile);

  std::string diffs[3] = {"opti", "pesi", "rand"};

  for (int j = 0; j < 5; j++) {
    for (int i = 0; i < count; i++) {  // * Count to praktycznie sizes[]
      for (int d = 0; d < 3; d++) {
        double avg = 0;
        displayName(sorts[j], resfile);
        std::cout << " " << diffs[d] << "  |";
        resfile << " " << diffs[d] << "  |";
        displaySize(sizes[i], resfile);

        std::string l;
        std::ifstream res("results/" + sorts[j] + "." +
                          std::to_string(sizes[i]) + "." + diffs[d]);
        while (getline(res, l)) {
          displayResNumber(std::stod(l), resfile);
          avg += std::stod(l);
        }

        std::cout << "\x1b[32m";
        displayResNumber(avg / repeats, resfile);
        std::cout << "\n";
        resfile << "\n";
        generateBorder(repeats, resfile);
      }
    }
    generateBorder(repeats, resfile);
  }

  resfile.close();
};