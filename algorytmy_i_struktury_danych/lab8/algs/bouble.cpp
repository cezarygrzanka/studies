double* bouble(double* t, int size) {
  bool s = false;
  do {
    s = false;

    for (int i = 1; i < size; i++) {
      if (t[i - 1] > t[i]) {
        float temp = t[i - 1];
        t[i - 1] = t[i];
        t[i] = temp;

        s = true;
      }
    }
  } while (s);
  return t;
}