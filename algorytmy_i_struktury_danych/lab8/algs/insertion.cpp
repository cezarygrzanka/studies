double* insertion(double* t, int size) {
  int i = 1;
  while (i < size) {
    int j = i;
    while (j > 0 && t[j - 1] > t[j]) {
      float temp = t[j - 1];
      t[j - 1] = t[j];
      t[j] = temp;
      j--;
    }
    i++;
  }

  return t;
}