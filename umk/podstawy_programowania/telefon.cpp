#include <iostream>
#include <string>

int main() {
  // Inicjalizacja tablic
  char str[160];
  int k[9];

  // Czyszczenie tablic
  for (int i = 0; i < 160; i++)
    str[i] = 0;
  for (int i = 0; i < 9; i++)
    k[i] = 0;

  // Wprowadzanie danych
  std::cout << "Podaj wiadomość (max 160 znaków): ";
  std::cin.getline(str, 160);

  for (int i = 0; i < 160; i++) {
    int c = (int)str[i];

    if ((c >= 'a' && c <= 'c') || (c >= 'A' && c <= 'C'))
      k[1]++;
    else if ((c >= 'd' && c <= 'f') || (c >= 'D' && c <= 'F'))
      k[2]++;
    else if ((c >= 'g' && c <= 'i') || (c >= 'G' && c <= 'I'))
      k[3]++;
    else if ((c >= 'j' && c <= 'l') || (c >= 'J' && c <= 'L'))
      k[4]++;
    else if ((c >= 'm' && c <= 'o') || (c >= 'M' && c <= 'O'))
      k[5]++;
    else if ((c >= 'p' && c <= 's') || (c >= 'P' && c <= 'S'))
      k[6]++;
    else if ((c >= 't' && c <= 'v') || (c >= 'T' && c <= 'V'))
      k[7]++;
    else if ((c >= 'w' && c <= 'z') || (c >= 'W' && c <= 'Z'))
      k[8]++;
    else if (c == ' ')
      k[0]++;
  }

  for (int i = 1; i < 9; i++) {
    if (k[i])
      std::cout << "[" << i + 1 << "]"
                << " - " << k[i] << "\n";
  }

  if (k[0])
    std::cout << "[0] - " << k[0] << "\n";

  return 0;
}