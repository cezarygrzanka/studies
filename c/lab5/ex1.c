#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Student Student;
struct Student
{
    char nazwisko[32];
    char data_ur[11];
    int nr_index;
    float ocena1;
    float ocena2;
    float ocena3;
};

Student **students;
int studentsCount;

void addStudents()
{
    int n = 0;

    printf("Podaj liczbe studentow: ");
    fflush(stdout);
    scanf(" %d", &n);

    studentsCount = n;

    students = (Student **)malloc(studentsCount * sizeof(struct Student *));

    for (int i = 0; i < studentsCount; i++)
    {
        students[i] = (Student *)malloc(studentsCount * sizeof(struct Student));

        char nazwisko[20], data_ur[11];
        int nr_index;
        float ocena1, ocena2, ocena3;

        printf("Podaj nazwisko: ");
        fflush(stdout);
        scanf("%s", nazwisko);
        strcpy(students[i]->nazwisko, nazwisko);

        printf("Podaj date urodzenia: ");
        fflush(stdout);
        scanf("%s", data_ur);
        strcpy(students[i]->data_ur, data_ur);

        printf("Podaj numer indeksu: ");
        fflush(stdout);
        scanf("%d", &nr_index);
        students[i]->nr_index = nr_index;

        printf("Podaj ocene z podstaw programowania: ");
        fflush(stdout);
        scanf("%f", &ocena1);
        students[i]->ocena1 = ocena1;

        printf("Podaj ocene z podstaw sys-operacyjnych: ");
        fflush(stdout);
        scanf("%f", &ocena2);
        students[i]->ocena2 = ocena2;

        printf("Podaj ocene z algorytmow i struktur danych: ");
        fflush(stdout);
        scanf("%f", &ocena3);
        students[i]->ocena3 = ocena3;
        printf("\n\n");
    }
}

void showStudents()
{
    printf("\n\n");
    for (int i = 0; i < studentsCount; i++)
    {
        printf("Nazwisko: %s\n", students[i]->nazwisko);
        printf("Data urodzenia: %s\n", students[i]->data_ur);
        printf("Numer indeksu: %i\n", students[i]->nr_index);
        printf("Podstawy programowania: %f\n", students[i]->ocena1);
        printf("Podstawy sys-op.: %f\n", students[i]->ocena2);
        printf("Alg. i str. danych: %f\n", students[i]->ocena3);
        printf("\n\n");
    }
}

void showStudent(int i)
{
    printf("\n------------\n");
    printf("Nazwisko: %s\n", students[i]->nazwisko);
    printf("Data urodzenia: %s\n", students[i]->data_ur);
    printf("Numer indeksu: %i\n", students[i]->nr_index);
    printf("Podstawy programowania: %f\n", students[i]->ocena1);
    printf("Podstawy sys-op.: %f\n", students[i]->ocena2);
    printf("Alg. i str. danych: %f\n", students[i]->ocena3);
    printf("------------\n");
}

void swapStudent(int i)
{
    for (i; i < studentsCount - 1; i++)
    {
        if (i < 0 && i >= studentsCount - 1)
        {
            printf("Wystapil blad przy usuwaniu studenta");
            return;
        }
    }

    students[i] = students[i + 1];
}

void removeStudent()
{
    char nazwisko[32];
    printf("\nPodaj nazwisko studenta, którego chcesz usunac: ");
    fflush(stdout);
    scanf(" %s", nazwisko);

    for (int i = 0; i < studentsCount; i++)
    {
        if (strcmp(students[i]->nazwisko, nazwisko) == 0)
        {
            swapStudent(i);
            free(students[studentsCount - 1]);
            studentsCount--;
            return;
        }
    }
}

void findStudentBySurname()
{
    char text[32];

    printf("Podaj nazwisko studenta ktorego chcesz wyszukac: ");
    fflush(stdout);
    scanf("%s", text);

    for (int i = 0; i < studentsCount; i++)
    {
        if (strcmp(text, students[i]->nazwisko) == 0)
        {
            showStudent(i);
            return;
        }
    }

    printf("Nie znaleziono studenta o nazwisku: %s", text);
}

void findHighestMark()
{
    int choose, max = 0, studentMax = 0, size = studentsCount;
    printf("Studenta z najwyzsza ocena z jakiego przedmiotu znalezc?\n");
    printf("Podstawy programowania (1), Podstawy sys-op (2), Alg. i str. danych (3): ");
    fflush(stdout);
    scanf("%d", &choose);
    printf("\n");

    switch (choose)
    {
    case 1:
        for (int i = 0; i < size; i++)
        {
            if (students[i]->ocena1 > max)
            {
                max = students[i]->ocena1;
                studentMax = i;
            }
        }
        printf("Najwyzsza ocene z podstaw programowania ma student o nazwisku: %s", students[studentMax]->nazwisko);
        break;
    case 2:
        for (int i = 0; i < size; i++)
        {
            if (students[i]->ocena2 > max)
            {
                max = students[i]->ocena2;
                studentMax = i;
            }
        }
        printf("Najwyzsza ocene z sys-op ma student o nazwisku: %s", students[studentMax]->nazwisko);
        break;
    case 3:
        for (int i = 0; i < size; i++)
        {
            if (students[i]->ocena3 > max)
            {
                max = students[i]->ocena3;
                studentMax = i;
            }
        }
        printf("Najwyzsza ocene z alg. i str. danych ma student o nazwisku: %s", students[studentMax]->nazwisko);
        break;

    default:
        break;
    }
    printf("\n");
}

void findHighestAvarage()
{
    float maxAvg = 0;
    int studentIndex;

    for (int i = 0; i < studentsCount; i++)
    {
        float tempAvg = (students[i]->ocena1 + students[i]->ocena2 + students[i]->ocena3) / 3;
        if (tempAvg > maxAvg)
        {
            maxAvg = tempAvg;
            studentIndex = i;
        }
    }

    printf("\nNajwyzsza srednia aocen ma student o nazwusku: %s, wynosi ona: %f\n", students[studentIndex]->nazwisko, maxAvg);
}

int posort(const void *a, const void *b)
{
    const Student *studentA = *(const Student **)a;
    const Student *studentB = *(const Student **)b;
    int res = strcmp(studentA->nazwisko, studentB->nazwisko);
    return res;
}

void sortStudents()
{
    qsort(students, studentsCount, sizeof(Student *), posort);
}

int main()
{
    int choose;

    addStudents();
    showStudents();

    do
    {
        printf("\nPodaj co chcesz zrobic?\n");
        printf("   1. Usun studenta\n");
        printf("   2. Wyszukaj studenta wg nazwiska\n");
        printf("   3. Posortowanie studentow alfabetycznie\n");
        printf("   4. Student ktory ma najwyzsza ocene z danego przedmiotu\n");
        printf("   5. Student z najwyzsza srednia ocen\n");
        printf("   6. \x1b[31mWyjdz z programu\x1b[0m\n");
        printf("A wiec?: ");
        fflush(stdout);
        scanf("%d", &choose);

        switch (choose)
        {
        case 1:
            removeStudent();
            showStudents();
            break;
        case 2:
            findStudentBySurname();
            break;
        case 3:
            sortStudents();
            showStudents();
            break;
        case 4:
            findHighestMark();
            break;
        case 5:
            findHighestAvarage();
            break;
        case 6:
            break;

        default:
            printf("Błędna opcja");
            break;
        }
    } while (choose != 6);

    return 0;
}